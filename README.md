# LSL tobii pro

This is a python script which allows to stream Tobii pro data through LSL. 

# Requirements
* python 3.5 (specific for the tobii_research module)
* pylsl
  ```bash
  pip install pylsl
  ```
* tobii-research
  ```bash
  pip install tobii-research
  ```